# Quarto slides test repository

This is a test repository for generating and hosting slides using [quarto](https://quarto.org).

The current version of the slides is [here](https://tobi_public.pages.gwdg.de/quarto_test/).

## What is quarto?

Quarto is a "A scientific and technical publishing system built on Pandoc" and [Pandoc](https://pandoc.org/) is a "a universal document converter".
While Pandoc provides a vast set of possibilities to convert documents from one format into another, Quarto helps with useful utilities and templates.

## What does this repository?

This reporitory contains a small example presentation written in markdown ([source](test.qmd)), which contains a few slides and some python code.
Quarto executes the python code and creates a [reveal.js](https://revealjs.com/)-based presentation out of this source, which is published on the [gitlab-pages server](https://tobi_public.pages.gwdg.de/quarto_test/).

The steps to install all required software and create the presentation can be found in the [CI configuration](.gitlab-ci.yml). Once you have everything installed, it's basically running
```
quarto render test.qmd
```

## More help

* [Quarto Getting Started](https://quarto.org/)
* [Quarto Revealjs output](https://quarto.org/docs/presentations/revealjs/)
* [Installing Quarto](https://quarto.org/docs/getting-started/installation.html)
